if(NOT PICO_SDK_PATH)
    message(FATAL_ERROR
            "SDK location was not specified. Please set PICO_SDK_PATH or set PICO_SDK_FETCH_FROM_GIT to on to fetch from git."
            )
endif()

file(COPY ${PICO_SDK_PATH}/src/rp2040/hardware_regs/rp2040.svd DESTINATION ${CMAKE_BINARY_DIR})