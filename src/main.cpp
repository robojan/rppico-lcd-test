#include <stdio.h>
#include <cstdint>
#include "pico/stdlib.h"
#include <initializer_list>
#include <vector>
#include <cassert>
#include <array>

void dumpVector(const std::vector<uint8_t> &buf)
{
    for (auto c : buf)
    {
        printf(" %02x", c);
    }
}

namespace pins
{
    namespace lcd
    {
        static constexpr int fmark = 26;
        static constexpr int cs = 2;
        static constexpr int rs = 3;
        static constexpr int wr = 4;
        static constexpr int rd = 5;
        static constexpr int rst = 6;
        static constexpr int d_offset = 7;
        static constexpr uint32_t d_mask = 0xFFFF << 7;
    }
}

namespace lcd
{
    static constexpr std::array<uint8_t, 6> c_expectedId{0, 2, 4, 0x94, 0x81, 0xff};
    static constexpr int c_width = 480;
    static constexpr int c_height = 320;
    namespace pinmasks
    {
        static constexpr uint32_t cmd_start_mask = (1 << pins::lcd::rs) | (1 << pins::lcd::cs) | (1 << pins::lcd::rd) | (1 << pins::lcd::wr);
        static constexpr uint32_t cmd_start_value = (0 << pins::lcd::rs) | (0 << pins::lcd::cs) | (1 << pins::lcd::rd) | (1 << pins::lcd::wr);
        static constexpr uint32_t cmd_end_mask = (1 << pins::lcd::rs) | (1 << pins::lcd::cs) | (1 << pins::lcd::rd) | (1 << pins::lcd::wr);
        static constexpr uint32_t cmd_end_value = (1 << pins::lcd::rs) | (1 << pins::lcd::cs) | (1 << pins::lcd::rd) | (1 << pins::lcd::wr);
        static constexpr uint32_t write_mask = pins::lcd::d_mask | (1 << pins::lcd::wr);
    }
    namespace commands
    {
        struct CmdInfo
        {
            uint8_t cmd;
            int8_t numParameters = 0;
            int8_t responseSize = 0;
        };
        static constexpr CmdInfo nop{0x00};
        static constexpr CmdInfo soft_reset{0x01};
        static constexpr CmdInfo exit_sleep_mode{0x11, 0, 0};
        static constexpr CmdInfo set_display_on{0x29, 0, 0};
        static constexpr CmdInfo set_column_address{0x2a, 4, 0};
        static constexpr CmdInfo set_page_address{0x2b, 4, 0};
        static constexpr CmdInfo write_memory_start{0x2c, 0, 0};
        static constexpr CmdInfo set_address_mode{0x36, 1, 0};
        static constexpr CmdInfo set_pixel_format{0x3a, 1, 0};
        static constexpr CmdInfo display_mode_and_fmem_mode{0xb4, 4, 0}; // Check
        static constexpr CmdInfo device_code_read{0xBF, 0, 6};
        static constexpr CmdInfo panel_drive_setting{0xc0, 6, 0};
        static constexpr CmdInfo frame_rate{0xc5, 1, 0};
        static constexpr CmdInfo interface_control{0xc6, 1, 0};
        static constexpr CmdInfo gamma_setting{0xc8, 12, 0};
        static constexpr CmdInfo power_setting{0xd0, 3, 0};
        static constexpr CmdInfo vcom_control{0xd1, 3, 0};
        static constexpr CmdInfo power_setting_normal{0xd2, 2, 0};
        static constexpr CmdInfo unknown_e4{0xe4, 1, 0};
        static constexpr CmdInfo unknown_f0{0xf0, 1, 0};
        static constexpr CmdInfo unknown_f3{0xf3, 2, 0};
    }

    void setup()
    {
        gpio_init(pins::lcd::fmark);
        gpio_init(pins::lcd::cs);
        gpio_init(pins::lcd::rs);
        gpio_init(pins::lcd::wr);
        gpio_init(pins::lcd::rd);
        gpio_init(pins::lcd::rst);
        gpio_init_mask(pins::lcd::d_mask);
        gpio_set_dir(pins::lcd::cs, true);
        gpio_set_dir(pins::lcd::rs, true);
        gpio_set_dir(pins::lcd::wr, true);
        gpio_set_dir(pins::lcd::rd, true);
        gpio_set_dir(pins::lcd::rst, true);
        gpio_put(pins::lcd::rst, false);
        gpio_put(pins::lcd::cs, true);
        gpio_put(pins::lcd::wr, true);
        gpio_put(pins::lcd::rd, true);
        gpio_put(pins::lcd::rs, true);
    }

    void reset()
    {
        gpio_put(pins::lcd::rst, false);
        sleep_ms(10);
        gpio_put(pins::lcd::rst, true);
        sleep_ms(1);
    }

    void enableOutputs()
    {
        gpio_set_dir_out_masked(pins::lcd::d_mask);
    }

    void disableOutputs()
    {
        gpio_set_dir_in_masked(pins::lcd::d_mask);
    }

    void writeWord(uint16_t val)
    {
        gpio_put_masked(pinmasks::write_mask, (val << pins::lcd::d_offset) | (0 << pins::lcd::wr));
        busy_wait_us(1);
        gpio_put(pins::lcd::wr, true);
        busy_wait_us(1);
    }

    uint16_t readWord()
    {
        gpio_put(pins::lcd::rd, false);
        busy_wait_us(1);
        uint16_t v = (gpio_get_all() & pins::lcd::d_mask) >> pins::lcd::d_offset;
        gpio_put(pins::lcd::rd, true);
        busy_wait_us(1);
        return v;
    }

    void cmd(const commands::CmdInfo &info, std::initializer_list<uint8_t> params = {}, std::vector<uint8_t> *resp = nullptr)
    {
        assert(params.size() == info.numParameters);
        assert(info.responseSize == 0 || (info.responseSize > 0 && resp));
        // Send command
        gpio_put_masked(pinmasks::cmd_start_mask, pinmasks::cmd_start_value);
        enableOutputs();
        busy_wait_us(1);
        writeWord(info.cmd);
        // Switch to data
        gpio_put(pins::lcd::rs, true);
        auto paramIt = params.begin();
        for (int i = 0; i < info.numParameters; i++)
        {
            writeWord(*paramIt);
            paramIt++;
        }
        if (resp)
            resp->clear();
        disableOutputs();
        for (int i = 0; i < info.responseSize; i++)
        {
            uint16_t w = readWord();
            resp->push_back(static_cast<uint8_t>(w));
        }
        gpio_put_masked(pinmasks::cmd_end_mask, pinmasks::cmd_end_value);
    }

    void setColumnAddress(int start, int end)
    {
        assert(start >= 0 && start < c_height);
        assert(end >= 0 && end < c_height);
        cmd(commands::set_column_address, {static_cast<uint8_t>((start >> 8) & 0xff),
                                           static_cast<uint8_t>((start >> 0) & 0xff),
                                           static_cast<uint8_t>((end >> 8) & 0xff),
                                           static_cast<uint8_t>((end >> 0) & 0xff)});
    }

    void setPageAddress(int start, int end)
    {
        assert(start >= 0 && start < c_width);
        assert(end >= 0 && end < c_width);
        cmd(commands::set_page_address, {static_cast<uint8_t>((start >> 8) & 0xff),
                                         static_cast<uint8_t>((start >> 0) & 0xff),
                                         static_cast<uint8_t>((end >> 8) & 0xff),
                                         static_cast<uint8_t>((end >> 0) & 0xff)});
    }

    void setAddress(int startX, int endX, int startY, int endY)
    {
        setColumnAddress(startY, endY);
        setPageAddress(startX, endX);
    }

    bool init()
    {
        std::vector<uint8_t> cmdBuffer;
        cmdBuffer.reserve(32);

        reset();

        cmd(commands::device_code_read, {}, &cmdBuffer);
        printf("LCD ID: ");
        dumpVector(cmdBuffer);
        if (std::equal(cmdBuffer.begin(), cmdBuffer.end(), c_expectedId.begin(), c_expectedId.end()))
        {
            printf(" OK\n");
        }
        else
        {
            printf(" Fail\n");
            return false;
        }

        // Init sequence:
        cmd(commands::soft_reset);
        sleep_ms(220);
        cmd(commands::exit_sleep_mode);
        sleep_ms(280);
        cmd(commands::power_setting, {0x08, 0x44, 0x1e});
        sleep_ms(220);
        cmd(commands::vcom_control, {0x00, 0x0c, 0x1a});
        cmd(commands::frame_rate, {0x03});
        cmd(commands::power_setting_normal, {0x01, 0x11});
        cmd(commands::unknown_e4, {0xa0});
        cmd(commands::unknown_f3, {0x00, 0x2a});
        cmd(commands::gamma_setting, {0x00, 0x26, 0x21, 0x00, 0x00, 0x1f, 0x65, 0x23, 0x77, 0x00, 0x0f, 0x00});
        cmd(commands::panel_drive_setting, {0x00, 0x3b, 0x00, 0x02, 0x11, 0x01});
        cmd(commands::interface_control, {0x83});
        cmd(commands::unknown_f0, {0x01});
        cmd(commands::unknown_e4, {0x0a});
        cmd(commands::set_address_mode, {0x8c});
        cmd(commands::set_pixel_format, {0x55});
        cmd(commands::display_mode_and_fmem_mode, {0x02, 0x00, 0x00, 0x01});
        sleep_ms(280);
        setColumnAddress(0, c_height - 1);
        setPageAddress(0, c_width - 1);
        cmd(commands::set_display_on);
        return true;
    }

    void fillRectangle(int startX, int endX, int startY, int endY, uint16_t color)
    {
        setAddress(startX, endX, startY, endY);
        int w = endX - startX + 1;
        int h = endY - startY + 1;
        int numColors = w * h;

        // Send command
        gpio_put_masked(pinmasks::cmd_start_mask, pinmasks::cmd_start_value);
        enableOutputs();
        busy_wait_us(1);
        writeWord(commands::write_memory_start.cmd);

        // Switch to data
        gpio_put(pins::lcd::rs, true);
        for (int i = 0; i < numColors; i++)
        {
            writeWord(color);
        }

        // Finish up
        disableOutputs();
        gpio_put_masked(pinmasks::cmd_end_mask, pinmasks::cmd_end_value);
    }

    void clear(uint16_t color)
    {
        fillRectangle(0, c_width - 1, 0, c_height - 1, color);
    }

}

int main()
{
    setup_default_uart();
    printf("Starting display\n");

    lcd::setup();
    while (true)
    {
        if (!lcd::init())
        {
            sleep_ms(10);
            continue;
        }
        lcd::clear(0xFFFF);
        lcd::fillRectangle(50, 200, 50, 200, 0xF800);
        sleep_ms(3000);
    }

    while (true)
        ;
    return 0;
}